ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.11.12"

lazy val root = (project in file("."))
  .settings(
    name := "vision_uc5"
  )

resolvers += "Hortonworks repo" at "https://repo.hortonworks.com/content/repositories/releases/"
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.2"
libraryDependencies += "com.hortonworks.hive" %% "hive-warehouse-connector" % "1.0.0.3.1.0.0-78"
libraryDependencies += "org.apache.spark" %% "spark-hive" % "2.3.2"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.3.2" % "provided"

scalacOptions += "-target:jvm-1.8"


